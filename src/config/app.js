module.exports = {
  environment: process.env.NODE_ENV || 'test',
  port: process.env.PORT || 8080,
  DOMAIN: process.env.APP_DOMAIN
}
