const database = require('../../database/models')

const { mockTeacher } =
  require('../../api/teachers/v1/command/console')

const { mockCourse } =
    require('../../api/courses/v1/command/console')

const mock = async () => {
  await mockTeacher(20)
  await mockCourse(20)
}

mock().then(() => {
  setTimeout(() => {
    database.sequelize.close()
  }, 3000)
})
