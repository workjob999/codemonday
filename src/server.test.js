const request = require('supertest')

const server = require('./server')

const HTTP_MESSAGE = require('./config/http-message')

describe('[INTEGRATION] Express Server', () => {
  describe('Error Handle', () => {
    it('should response with HTTP 404 Url Not Found', (done) => {
      request(server)
        .get('/notfound')
        .expect(HTTP_MESSAGE.CLIENT_ERROR.NOT_FOUND.CODE)
        .expect((res) => {
          (HTTP_MESSAGE.CLIENT_ERROR.NOT_FOUND.MESSAGE = res.body)
        })
        .end(done)
    })
  })
})
