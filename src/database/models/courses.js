const Sequelize = require('sequelize')

module.exports = (sequelize, DataTypes) => {
  const Courses = sequelize.define(
    'courses',
    {
      id: {
        type: DataTypes.UUID,
        allowNull: false,
        primaryKey: true,
        unique: true,
        defaultValue: DataTypes.UUIDV4
      },
      teacher_id: {
        type: Sequelize.STRING,
        allowNull: true
      },
      course_name: {
        type: Sequelize.STRING,
        allowNull: true
      },
      course_description: {
        type: Sequelize.STRING,
        allowNull: true
      },
      status: {
        type: Sequelize.BOOLEAN,
        allowNull: true,
        defaultValue: true
      }
    },
    {
      timestamps: true,
      createdAt: 'created_at',
      updatedAt: 'updated_at'
    }
  )

  Courses.associate = (models) => {
    Courses.belongsTo(models.teachers, {
      foreignKey: 'teacher_id'
    })
  }

  return Courses
}
