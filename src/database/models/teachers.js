const Sequelize = require('sequelize')

module.exports = (sequelize, DataTypes) => {
  const Teachers = sequelize.define(
    'teachers',
    {
      id: {
        type: DataTypes.UUID,
        allowNull: false,
        primaryKey: true,
        unique: true,
        defaultValue: DataTypes.UUIDV4
      },
      first_name: {
        type: Sequelize.STRING,
        allowNull: true
      },
      last_name: {
        type: Sequelize.STRING,
        allowNull: true
      },
      age: {
        type: Sequelize.INTEGER,
        allowNull: true
      },
      status: {
        type: Sequelize.BOOLEAN,
        allowNull: true,
        defaultValue: true
      }
    },
    {
      timestamps: true,
      createdAt: 'created_at',
      updatedAt: 'updated_at'
    }
  )

  Teachers.associate = (models) => {
    Teachers.hasMany(models.courses, {
      foreignKey: 'teacher_id'
    })
  }

  return Teachers
}
