module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.createTable('teachers', {
    id: {
      type: Sequelize.UUID,
      allowNull: false,
      primaryKey: true
    },
    first_name: {
      type: Sequelize.STRING,
      allowNull: true
    },
    last_name: {
      type: Sequelize.STRING,
      allowNull: true
    },
    age: {
      type: Sequelize.INTEGER,
      allowNull: true
    },
    status: {
      type: Sequelize.BOOLEAN,
      allowNull: true,
      defaultValue: true
    },
    created_at: {
      allowNull: false,
      type: Sequelize.DATE,
      defaultValue: Sequelize.NOW
    },
    updated_at: {
      type: Sequelize.DATE,
      allowNull: false,
      defaultValue: Sequelize.NOW
    }
  }),
  down: (queryInterface) => queryInterface.dropTable('teachers')
}
