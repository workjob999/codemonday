module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.createTable('courses', {
    id: {
      type: Sequelize.UUID,
      allowNull: false,
      primaryKey: true
    },
    teacher_id: {
      type: Sequelize.STRING,
      allowNull: true
    },
    course_name: {
      type: Sequelize.STRING,
      allowNull: true
    },
    course_description: {
      type: Sequelize.STRING,
      allowNull: true
    },
    status: {
      type: Sequelize.BOOLEAN,
      allowNull: true
    },
    created_at: {
      allowNull: false,
      type: Sequelize.DATE,
      defaultValue: Sequelize.NOW
    },
    updated_at: {
      allowNull: false,
      type: Sequelize.DATE,
      defaultValue: Sequelize.NOW
    }
  }),
  down: (queryInterface) => queryInterface.dropTable('tickets')
}
