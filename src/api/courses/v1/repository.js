const APP_VARIABLES = require('../../../config/variables')

const Teachers = require('../../../database/models').teachers
const Courses = require('../../../database/models').courses

class CourseRepository {
  static async getCourses (where = null,
    orderBy = ['created_at', 'DESC'],
    limit = APP_VARIABLES.LIMIT,
    offset = 1) {
    const filterPage = {
      offset,
      limit
    }

    const include = {
      model: Teachers,
      attributes: ['id', 'first_name', 'last_name', 'age']
    }

    const filter = {
      where: {},
      include: [include],
      order: [
        orderBy
      ],
      ...filterPage
    }

    if (where) {
      Object.assign(filter.where, where)
    }

    const courses = await Courses.findAll(filter)

    return courses
  }

  static async findCourse (filter) {
    const course = await Courses.findOne({
      where: filter
    })

    return course
  }

  static async createCourse (courseData) {
    return await Courses.create(courseData)
  }
}

module.exports = CourseRepository
