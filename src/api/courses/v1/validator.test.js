const { expect } = require('chai')

const {
  requireStrWithLength,
  isStatus
} = require('./validator')

describe('[UNIT] test validator', () => {
  describe('Validator first_name', () => {
    const value = 'test'

    it('Should be equal true when correct condition', () => {
      expect(requireStrWithLength(value, 10)).to.equal(true)
    })

    it('Should be equal false when not correct condition', () => {
      expect(requireStrWithLength(value, 2)).to.equal(false)
    })
  })

  describe('Validator Status', () => {
    const status = true

    it('Should be equal true when correct status', () => {
      expect(isStatus(status)).to.equal(true)
    })

    const notValidStatus = 'test'

    it('Should be equal false when not correct status', () => {
      expect(isStatus(notValidStatus)).to.equal(false)
    })
  })
})
