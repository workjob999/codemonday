const { expect } = require('chai')
const APP_VARIABLES = require('../../../config/variables')
const faker = require('faker')
const CourseServices = require('./service')

const courseServices = new CourseServices()

describe('[UNIT] test service', () => {
  describe('Get Course', () => {
    const conditionCourse = {
      status: null,
      startDate: null,
      endDate: null,
      order: 'created_at',
      ascdesc: 'DESC',
      limit: APP_VARIABLES.LIMIT,
      page: 1
    }

    it('Should be correct get course have all filter', async () => {
      const courses = await courseServices.getCourses(conditionCourse)
      expect(courses).not.to.equal(null)
    })
  })

  describe('Create course', () => {
    it('Should be not null when data is correct', async () => {
      const dataCourse = {
        teacher_id: faker.datatype.uuid(),
        course_name: faker.random.words(),
        course_description: faker.lorem.paragraphs(),
        status: faker.datatype.boolean(),
        created_at: faker.date.between('2018-01-01', '2020-12-31')
      }

      const course = await courseServices.createCourse(dataCourse)

      expect(course).not.to.equal(null)
    })
  })
})
