const faker = require('faker')

const { expect } = require('chai')

const CourseRepository = require('./repository')

describe('[UNIT] test Repository', () => {
  describe('Get Courses', () => {
    it('Should be pass when get courses no filter', async () => {
      const courses = await CourseRepository.getCourses()
      expect(courses).not.to.equal(null)
    })
  })

  describe('Get Courses (result = null)', () => {
    it('Should be pass when get courses no filter', async () => {
      const where = {
        course_name: 'testssssss'
      }
      const courses = await CourseRepository.getCourses(where)
      expect(courses).not.to.equal(null)
    })
  })

  describe('Create course', () => {
    it('Should be not null when data is correct', async () => {
      const dataCourse = {
        teacher_id: faker.datatype.uuid(),
        course_name: faker.random.words(),
        course_description: faker.lorem.paragraphs(),
        status: faker.datatype.boolean(),
        created_at: faker.date.between('2018-01-01', '2020-12-31')
      }

      const course = await CourseRepository.createCourse(dataCourse)

      expect(course).not.to.equal(null)
    })
  })
})
