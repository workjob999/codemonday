/* eslint-disable no-await-in-loop */
const faker = require('faker')

const Repository = require('../repository')
const TeacherRepository = require('../../../teachers/v1/repository')

module.exports.mockCourse = async (totalCourses) => {
  for (let i = 1; i <= totalCourses; i += 1) {
    const totalTeacher = await TeacherRepository.getCountTeacher()
    const randomIndex = faker.datatype.number(totalTeacher)
    const orderBy = ['created_at', 'DESC']

    const teacher = await TeacherRepository.getTeachers(null, orderBy, 1, randomIndex)

    const course = {
      teacher_id: teacher ? teacher[0].id : null,
      course_name: faker.random.words(),
      course_description: faker.lorem.paragraphs(),
      status: faker.datatype.boolean(),
      created_at: faker.date.between('2018-01-01', '2020-12-31')
    }

    await Repository.createCourse(course)
  }
}
