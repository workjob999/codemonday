const HTTP_MESSAGE = require('../../../config/http-message')
const API_RESPONSE_MESSAGE = require('../../../api/response-message')
const APP_VARIABLES = require('../../../config/variables')
const COURSE_RESPONSE_MESSAGE = require('./error-message')
const ErrorHandle = require('../../../api/error')

const { requireStrWithLength, isStatus } = require('./validator')

const ErrorApiManager = require('../../error/api')

const CourseServices = require('./service')
const courseServices = new CourseServices()

exports.getCourses = async (req, res) => {
  try {
    const {
      limit = APP_VARIABLES.LIMIT,
      page = 1,
      order = 'created_at',
      ascdesc = 'DESC'
    } = req.query

    const conditionCourse = {
      order: order,
      ascdesc: ascdesc,
      limit: limit,
      page: page
    }

    const courses = await courseServices.getCourses(conditionCourse)

    return res.status(HTTP_MESSAGE.SUCCESS.OK.CODE).json({
      code: HTTP_MESSAGE.SUCCESS.OK.CODE,
      status: API_RESPONSE_MESSAGE.SUCCESS.MESSAGE,
      courses
    })
  } catch (err) {
    return ErrorApiManager.handle(err, res)
  }
}

exports.createCourse = async (req, res) => {
  console.log(req.body)
  try {
    const {
      teacherId = null,
      courseName = null,
      courseDescription = null,
      status = true
    } = req.body

    if (!requireStrWithLength(teacherId, 100)) {
      throw new ErrorHandle(null, COURSE_RESPONSE_MESSAGE.INVALID_PARAMS)
    }

    if (!requireStrWithLength(courseName, 100)) {
      throw new ErrorHandle(null, COURSE_RESPONSE_MESSAGE.INVALID_PARAMS)
    }

    if (!requireStrWithLength(courseDescription, 255)) {
      throw new ErrorHandle(null, COURSE_RESPONSE_MESSAGE.INVALID_PARAMS)
    }

    if (!isStatus(status)) {
      throw new ErrorHandle(null, COURSE_RESPONSE_MESSAGE.INVALID_STATUS)
    }

    const createDataCourse = {
      teacher_id: teacherId,
      course_name: courseName,
      course_description: courseDescription,
      status: status
    }

    const course = await courseServices.createCourse(createDataCourse)

    return res.status(HTTP_MESSAGE.SUCCESS.OK.CODE).json({
      code: HTTP_MESSAGE.SUCCESS.OK.CODE,
      status: API_RESPONSE_MESSAGE.SUCCESS.MESSAGE,
      course
    })
  } catch (err) {
    return ErrorApiManager.handle(err, res)
  }
}
