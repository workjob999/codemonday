const express = require('express')
const controller = require('./controller')

const router = express.Router()

router.get('/courses', controller.getCourses)
router.post('/course', controller.createCourse)
// router.post('/course', () => { console.log('test') } )

module.exports = router
