const Repository = require('./repository')
const APP_VARIABLES = require('../../../config/variables')

class CourseServices {
  async getCourses (conditionCourse) {
    const {
      order = 'created_at',
      ascdesc = 'DESC',
      limit = APP_VARIABLES.LIMIT,
      page = 1
    } = conditionCourse

    const filter = []

    const offset = ((page - 1) * limit)
    const limitPage = parseInt(limit)

    const orderBy = [order, ascdesc]

    const courses = await Repository.getCourses(
      filter,
      orderBy,
      limitPage,
      offset
    )

    return courses
  }

  async createCourse (createDataCourse) {
    return await Repository.createCourse(createDataCourse)
  }
}

module.exports = CourseServices
