exports.requireStrWithLength = (val, length = 20) => {
  if (!val || typeof val !== 'string') {
    return false
  }

  return val.length >= 1 && val.length <= length
}

exports.isStatus = (status) => {
  if (typeof status !== 'boolean') {
    return false
  }

  return true
}
