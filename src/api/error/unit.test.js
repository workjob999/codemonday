const ErrorHandle = require('../error')

const { expect } = require('chai')

describe('[UNIT] test ErrorApiManager and ErrorHandle', () => {
  it('Should get error api with no message condition', () => {
    const { STATUS, MESSAGE } = new ErrorHandle(null).getResponseMessage()
    expect(STATUS).to.equal('bad_request')
    expect(MESSAGE).to.equal('bad_request')
  })
})
