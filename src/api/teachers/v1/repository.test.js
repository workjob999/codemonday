const faker = require('faker')

const { expect } = require('chai')

const TeacherRepository = require('./repository')

describe('[UNIT] test Repository', () => {
  describe('Get Teachers', () => {
    it('Should be pass when get teachers no filter', async () => {
      const teachers = await TeacherRepository.getTeachers()
      expect(teachers).not.to.equal(null)
    })
  })

  describe('Get Teachers (result = null)', () => {
    it('Should be pass when get teachers no filter', async () => {
      const where = {
        first_name: 'testssssss'
      }
      const teachers = await TeacherRepository.getTeachers(where)
      expect(teachers).not.to.equal(null)
    })
  })

  describe('Create teacher', () => {
    it('Should be not null when data is correct', async () => {
      const dataTeacher = {
        first_name: faker.name.firstName(),
        last_name: faker.name.lastName(),
        age: faker.datatype.number(100),
        status: faker.datatype.boolean(),
        created_at: faker.date.between('2018-01-01', '2020-12-31')
      }

      const teacher = await TeacherRepository.createTeacher(dataTeacher)

      expect(teacher).not.to.equal(null)
    })
  })
})
