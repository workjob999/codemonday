const express = require('express')
const controller = require('./controller')

const router = express.Router()

router.get('/teachers', controller.getTeachers)
router.post('/teacher', controller.createTeacher)
router.get('/teacher/:teacherId/courses', controller.getTeacherCourses)

module.exports = router
