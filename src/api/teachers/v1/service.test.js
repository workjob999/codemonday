const { expect } = require('chai')
const APP_VARIABLES = require('../../../config/variables')
const faker = require('faker')
const TeacherServices = require('./service')

const teacherServices = new TeacherServices()

describe('[UNIT] test service', () => {
  describe('Get Teacher', () => {
    const conditionTeacher = {
      status: null,
      startDate: null,
      endDate: null,
      order: 'created_at',
      ascdesc: 'DESC',
      limit: APP_VARIABLES.LIMIT,
      page: 1
    }

    it('Should be correct get teacher have all filter', async () => {
      const teachers = await teacherServices.getTeachers(conditionTeacher)
      expect(teachers).not.to.equal(null)
    })
  })

  describe('Create teacher', () => {
    it('Should be not null when data is correct', async () => {
      const dataTeacher = {
        fullname: faker.name.findName(),
        detail: faker.lorem.text(),
        contact_information: faker.address.streetAddress(),
        status: faker.random.arrayElement(['Pending', 'Accepted', 'Resolved', 'Rejected']),
        created_at: faker.date.between('2018-01-01', '2020-12-31')
      }

      const teacher = await teacherServices.createTeacher(dataTeacher)

      expect(teacher).not.to.equal(null)
    })
  })
})
