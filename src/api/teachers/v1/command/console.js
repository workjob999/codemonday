/* eslint-disable no-await-in-loop */
const faker = require('faker')

const Repository = require('../repository')

module.exports.mockTeacher = async (totalTeachers) => {
  for (let i = 1; i <= totalTeachers; i += 1) {
    const teacher = {
      first_name: faker.name.firstName(),
      last_name: faker.name.lastName(),
      age: faker.datatype.number(100),
      status: faker.datatype.boolean(),
      created_at: faker.date.between('2018-01-01', '2020-12-31')
    }

    await Repository.createTeacher(teacher)
  }
}
