const Repository = require('./repository')
const APP_VARIABLES = require('../../../config/variables')

class TeacherServices {
  async getTeachers (conditionTeacher) {
    const {
      id = null,
      order = 'created_at',
      ascdesc = 'DESC',
      limit = APP_VARIABLES.LIMIT,
      page = 1
    } = conditionTeacher

    const filter = []

    if (id) {
      Object.assign(filter, {
        id
      })
    }

    const offset = ((page - 1) * limit)
    const limitPage = parseInt(limit)

    const orderBy = [order, ascdesc]

    const teachers = await Repository.getTeachers(
      filter,
      orderBy,
      limitPage,
      offset
    )

    return teachers
  }

  async createTeacher (createDataTeacher) {
    return await Repository.createTeacher(createDataTeacher)
  }
}

module.exports = TeacherServices
