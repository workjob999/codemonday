const APP_VARIABLES = require('../../../config/variables')

const Teachers = require('../../../database/models').teachers
const Courses = require('../../../database/models').courses

class TeacherRepository {
  static async getTeachers (where = null,
    orderBy = ['created_at', 'DESC'],
    limit = APP_VARIABLES.LIMIT,
    offset = 1) {
    const filterPage = {
      offset,
      limit
    }

    const include = {
      model: Courses,
      attributes: ['id', 'course_name', 'course_description']
    }

    // random && random !== null ? [orderBy] :
    const filter = {
      where: {},
      include: [include],
      order: [
        orderBy
      ],
      ...filterPage
    }

    if (where) {
      Object.assign(filter.where, where)
    }

    const teachers = await Teachers.findAll(filter)

    return teachers
  }

  static async findTeacher (filter) {
    const teacher = await Teachers.findOne({
      where: filter
    })

    return teacher
  }

  static async createTeacher (teacherData) {
    return await Teachers.create(teacherData)
  }

  static async getCountTeacher () {
    const teacher = await Teachers.count()

    return teacher
  }
}

module.exports = TeacherRepository
