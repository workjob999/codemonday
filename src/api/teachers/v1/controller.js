const HTTP_MESSAGE = require('../../../config/http-message')
const API_RESPONSE_MESSAGE = require('../../../api/response-message')
const APP_VARIABLES = require('../../../config/variables')
const TEACHER_RESPONSE_MESSAGE = require('./error-message')
const ErrorHandle = require('../../../api/error')

const { requireStrWithLength, isAge, isStatus } = require('./validator')

const ErrorApiManager = require('../../error/api')

const TeacherServices = require('./service')
const teacherServices = new TeacherServices()

exports.getTeachers = async (req, res) => {
  try {
    const {
      limit = APP_VARIABLES.LIMIT,
      page = 1,
      order = 'created_at',
      ascdesc = 'DESC'
    } = req.query

    const conditionTeacher = {
      order: order,
      ascdesc: ascdesc,
      limit: limit,
      page: page
    }

    const teachers = await teacherServices.getTeachers(conditionTeacher)

    return res.status(HTTP_MESSAGE.SUCCESS.OK.CODE).json({
      code: HTTP_MESSAGE.SUCCESS.OK.CODE,
      status: API_RESPONSE_MESSAGE.SUCCESS.MESSAGE,
      teachers
    })
  } catch (err) {
    return ErrorApiManager.handle(err, res)
  }
}

exports.createTeacher = async (req, res) => {
  try {
    const {
      firstName = null,
      lastName = null,
      age = null,
      status = true
    } = req.body

    if (!requireStrWithLength(firstName, 100)) {
      throw new ErrorHandle(null, TEACHER_RESPONSE_MESSAGE.INVALID_PARAMS)
    }

    if (!requireStrWithLength(lastName, 255)) {
      throw new ErrorHandle(null, TEACHER_RESPONSE_MESSAGE.INVALID_PARAMS)
    }

    if (!isAge(age)) {
      throw new ErrorHandle(null, TEACHER_RESPONSE_MESSAGE.INVALID_PARAMS)
    }

    if (!isStatus(status)) {
      throw new ErrorHandle(null, TEACHER_RESPONSE_MESSAGE.INVALID_STATUS)
    }

    const createDataTeacher = {
      first_name: firstName,
      last_name: lastName,
      age: age,
      status: status
    }

    const teacher = await teacherServices.createTeacher(createDataTeacher)

    return res.status(HTTP_MESSAGE.SUCCESS.OK.CODE).json({
      code: HTTP_MESSAGE.SUCCESS.OK.CODE,
      status: API_RESPONSE_MESSAGE.SUCCESS.MESSAGE,
      teacher
    })
  } catch (err) {
    return ErrorApiManager.handle(err, res)
  }
}

exports.getTeacherCourses = async (req, res) => {
  try {
    const teacherId = req.params.teacherId

    const conditionTeacher = {
      id: teacherId
    }

    const teachers = await teacherServices.getTeachers(conditionTeacher)

    return res.status(HTTP_MESSAGE.SUCCESS.OK.CODE).json({
      code: HTTP_MESSAGE.SUCCESS.OK.CODE,
      status: API_RESPONSE_MESSAGE.SUCCESS.MESSAGE,
      cousers: teachers[0].courses
    })
  } catch (err) {
    return ErrorApiManager.handle(err, res)
  }
}
