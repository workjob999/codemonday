const express = require('express')

const router = express.Router()

const teachers = require('../api/teachers/v1/route')
const courses = require('../api/courses/v1/route')

router.use('/v1', teachers)
router.use('/v1', courses)

module.exports = router
