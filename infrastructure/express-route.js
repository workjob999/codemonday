const express = require('express')
const helmet = require('helmet')

const api = require('../src/api/route')

const router = express.Router()

router.use(helmet())
router.use('/api', api)

module.exports = router
