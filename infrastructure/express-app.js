const express = require('express')
const cookieParser = require('cookie-parser')
const bodyParser = require('body-parser')
const cors = require('cors')
const Sentry = require('@sentry/node')
const Tracing = require('@sentry/tracing')
const expressRoute = require('./express-route')

const {
  handleURLNotFound,
  handleError,
  handleErrorJson
} = require('./express-error-handle')

const { SENTRY_DSN, environment } = require('../src/config/app')

const app = express()

class ExpressApp {
  constructor () {
    this.app = app
    this.environment = environment
  }

  init () {
    Sentry.init({
      dsn: SENTRY_DSN,
      integrations: [
        new Sentry.Integrations.Http({ tracing: true }),
        new Tracing.Integrations.Express({
          app
        })
      ],
      environment: this.environment,
      tracesSampleRate: 1.0
    })

    this.app.use(Sentry.Handlers.requestHandler())
    this.app.use(bodyParser.json())
    this.app.use(bodyParser.urlencoded({ extended: true }))
    this.app.use(cookieParser())
    this.app.use(cors())

    this.app.use('/static', express.static('public'))
    this.app.use('/', expressRoute)
    this.app.use(handleURLNotFound)
    this.app.use(handleErrorJson)
    this.app.use(handleError)
    this.app.use(Sentry.Handlers.errorHandler())
  }

  allowHeaders (headers) {
    this.app.use((req, res, next) => {
      res.header('Access-Control-Allow-Headers', headers)
      next()
    })
  }

  getApp () {
    return this.app
  }
}

module.exports = ExpressApp
