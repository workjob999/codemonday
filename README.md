# Teacher

This is a quick plug-and-play setup for your `Teacher` project

#### Comes with:
- `Language: Node-10.16.0`
- `Framework: Express-4.17.1`
- `Database: Mysql-8.0.3`
- `Unit test and Integration test - Mocha, Chai`
- `Other - Yarn, SOLID Principles`

#### All Command Migration & Factory For Mock Data (ฉบับรวดเร็ว)
- `Create mysql db`
- `cp .env.example .env`
- `change config .env`
- `/bin/sh init.sh`

#### Install and config (manual)
- `cp .env.example .env`
- `change config .env`
- `yarn`
- `yarn db:migrate`
- `yarn flow:run`

#### Run Unit test and Integration test
- `yarn test`

#### Run Eslint
- `yarn eslint`