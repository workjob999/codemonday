module.exports = {
  env: {
    jest: true
  },
  extends: [
    'standard'
  ],
  globals: {
    describe: true,
    it: true,
    before: true,
    beforeEach: true
  },
  rules: {}
}
